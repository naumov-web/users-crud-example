package com.naumov.userscrud.services.contracts;

import com.naumov.userscrud.dto.requests.CreateUserDTO;
import com.naumov.userscrud.dto.requests.UpdateUserDTO;
import com.naumov.userscrud.models.User;

import java.util.List;
import java.util.Optional;

public interface IUsersService {

    Optional<User> getUserById(Long id);

    List<User> getUsers(Integer size, Integer pageNumber, String sortBy, String sortDirection);

    void createUser(CreateUserDTO dto);

    void updateUser(User user, UpdateUserDTO dto);

}
