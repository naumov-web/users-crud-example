package com.naumov.userscrud.services;

import com.naumov.userscrud.dto.requests.CreateUserDTO;
import com.naumov.userscrud.dto.requests.UpdateUserDTO;
import com.naumov.userscrud.models.User;
import com.naumov.userscrud.services.contracts.IUsersService;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.Optional;

import com.naumov.userscrud.models.users_crud.tables.Users;

@Service
public class UsersService implements IUsersService {

    DSLContext context;

    BCryptPasswordEncoder passwordEncoder;
    @Autowired
    public UsersService(DSLContext context, BCryptPasswordEncoder passwordEncoder) {
        this.context = context;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return Optional.empty();
    }

    @Override
    public List<User> getUsers(Integer size, Integer pageNumber, String sortBy, String sortDirection) {
        return null;
    }

    @Override
    public void createUser(CreateUserDTO dto) {
        context.insertInto(Users.USERS)
                        .set(Users.USERS.EMAIL, dto.getEmail())
                        .set(Users.USERS.PASSWORD, passwordEncoder.encode(dto.getPassword()))
                        .set(Users.USERS.FULLNAME, dto.getFullName())
                        .set(Users.USERS.BIRTHDATA, dto.getBirthDate())
                        .set(Users.USERS.POSITION, dto.getPosition())
                        .execute();
//        context.insertInto(
//                    Users.USERS,
//                    Users.USERS.EMAIL,
//                    Users.USERS.PASSWORD,
//                    Users.USERS.FULLNAME,
//                    Users.USERS.BIRTHDATA,
//                    Users.USERS.POSITION
//                )
//                .values(
//                        dto.getEmail(),
//                        passwordEncoder.encode(dto.getPassword()),
//                        dto.getFullName(),
//                        dto.getBirthDate(),
//                        dto.getPosition()
//                );
    }

    @Override
    public void updateUser(User user, UpdateUserDTO dto) {

    }
}
