package com.naumov.userscrud.controllers;

import com.naumov.userscrud.dto.requests.CreateUserDTO;
import com.naumov.userscrud.dto.requests.UpdateUserDTO;
import com.naumov.userscrud.models.User;
import com.naumov.userscrud.services.contracts.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UsersController extends BaseController {

    @Autowired
    private IUsersService usersService;

    @PostMapping(value = "")
    public ResponseEntity create(@Valid @RequestBody final CreateUserDTO requestDto, final BindingResult binding) {
        if (binding.hasErrors()) {
            return ResponseEntity.badRequest().body(mapBindingToResource(binding));
        }

        usersService.createUser(requestDto);

        Map<Object, Object> response = new HashMap<>();
        response.put("success", true);
        response.put("message", "User was created!");

        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity index(
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(name = "sortBy", required = false) String sortBy,
            @RequestParam(name = "sortDirection", required = false) String sortDirection
    ) {
        return ResponseEntity.ok(null);
    }

    @PatchMapping(value = "{id}")
    public ResponseEntity update(
            @PathVariable(name = "id") Long id,
            @Valid @RequestBody final UpdateUserDTO requestDto,
            final BindingResult binding
    ) {
        if (binding.hasErrors()) {
            return ResponseEntity.badRequest().body(mapBindingToResource(binding));
        }

        Optional<User> user = usersService.getUserById(id);

        if (user.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        usersService.updateUser(
                user.get(),
                requestDto
        );

        return ResponseEntity.ok(null);
    }
}
