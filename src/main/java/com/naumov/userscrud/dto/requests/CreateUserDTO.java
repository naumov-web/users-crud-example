package com.naumov.userscrud.dto.requests;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
public class CreateUserDTO {
    @NotNull(message = "Email cannot be null")
    private final String email = null;

    @NotNull(message = "Password cannot be null")
    private final String password = null;

    @NotNull(message = "Full name cannot be null")
    private final String fullName = null;

    @NotNull(message = "Full name cannot be null")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private final Date birthDate = null;

    @NotNull(message = "Position cannot be null")
    private String position;
}
