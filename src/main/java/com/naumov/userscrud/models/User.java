package com.naumov.userscrud.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public final class User extends BaseModel{

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "fullName")
    private String fullName;

    @Column(name = "birthDate")
    private Date birthDate;

    @Column(name = "position")
    private String position;

}
